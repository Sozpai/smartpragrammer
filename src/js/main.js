// sourceMappingURL=main.js.map

// Получаем все кнопки
const buttons = document.querySelectorAll(".modal-button");

// Получаем модальное окно
const modal = document.getElementById("modal-window");

// Функция для открытия модального окна
function openModal() {
  modal.classList.add("open");
  document.addEventListener("keydown", closeModalOnEscape);
  modal.addEventListener("click", closeModalOnOutsideClick);
}

// Функция для закрытия модального окна
function closeModal() {
  modal.classList.remove("open");
  document.removeEventListener("keydown", closeModalOnEscape);
  modal.removeEventListener("click", closeModalOnOutsideClick);
}

function closeModalOnEscape(event) {
  if (event.key === "Escape") {
    closeModal();
  }
}

function closeModalOnOutsideClick(event) {
  if (event.target === modal) {
    closeModal();
  }
}
// Добавляем обработчики событий ко всем кнопкам
buttons.forEach((button) => {
  button.addEventListener("click", openModal);
});

// Добавляем обработчик события для закрытия модального окна
document.getElementById("close-modal").addEventListener("click", closeModal);
